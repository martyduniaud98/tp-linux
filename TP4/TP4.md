# TP4 : Une distribution orientée serveur

# I - Checklist

- **Choisir et définir une ip à la VM**

    - commande : `cat /etc/sysconfig/network-scripts/ifcfg-enp0s8`

    - résultat : 
    ```
    TYPE=Ethernet
    BOOTPROTO=static
    NAME=enp0s8
    DEVICE=enp0s8
    ONBOOT=yes
    IPADDR=10.200.1.89
    NETMASK=255.255.255.0
    ``` 
    
    - commande : `ip a`

    - résultat :
    ```
    3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ad:6f:2d brd ff:ff:ff:ff:ff:ff
    inet 10.200.1.89/24 brd 10.200.1.255 scope global noprefixroute enp0s8
    ```

- **Vérification du status service ssh**

    - commande : `systemctl status sshd`

    - résultat : 
    ```
    ● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Tue 2021-11-23 10:58:19 CET; 55min ago
   [...]
    ```

- **Connexion à la VM via échange de clés**

    - commande (cadenas) : `cat /home/marty/.ssh/authorized_keys`

    - résultat :
    ```
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILrvM0STZYfWUy95fST+QIqv24RiYlKQMwyAr3W9iBnK marty.duniaud@ynov.com
    ```

    - commande (clé) : `cat /home/marty/.ssh/id_rsa`

    - résultat : 
    ```
    -----BEGIN OPENSSH PRIVATE KEY-----
    b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAACFwAAAAdzc2gtcn
    NhAAAAAwEAAQAAAgEAyjGNQvIk8guhIAhuAeSvITmB91OehrAwFS0A6oPFREMOwysAD0cs
    qq73QZpbLzGclRXWZy/ufZsS2trvaCvlRnsHIOogsdb1O9pyGMQm0/uxtAgFjgGt/1NiH1
    [...]
    ZWzjrJDaPiJ8Zp6ogCUC/1DAc/y/42HsTtHormK1h9bnVLdVpQXaZoV0em4QrInO6YtQ2i
    GL+1m56QX+R4KIvn4gfU13s4V5a7L8Nsh8ykZrmBhFHOsNHOKwXIwPl68XW9QoYLXReMoC
    PYk0UoIdR2oHAAAAG21hcnR5QGxvY2FsaG9zdC5sb2NhbGRvbWFpbgECAwQFBgc=
    -----END OPENSSH PRIVATE KEY-----
    ```

- **Preuve d'accès internet avec un ping adress ip publique**

    - commande : `ping 8.8.8.8`

    - résultat : 
    ```
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=27.3 ms
    64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=29.5 ms
    64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=28.2 ms
    [...]
    ```

- **preuve d'accès internet avec un ping nom de domaine**

    - commande : `ping google.com`

    - résultat : 
    ```
    PING google.com (216.58.213.78) 56(84) bytes of data.
    64 bytes from lhr25s01-in-f78.1e100.net (216.58.213.78): icmp_seq=1 ttl=113 time=26.1 ms
    64 bytes from lhr25s01-in-f78.1e100.net (216.58.213.78): icmp_seq=2 ttl=113 time=27.10 ms
    64 bytes from lhr25s01-in-f78.1e100.net (216.58.213.78): icmp_seq=3 ttl=113 time=27.5 ms
    ```

- **Définition d'un nouveau nom de machine**

    - commande : `cat /etc/hostname`

    - résultat : 
    ```
    node1.tp4.linux
    ```

    - commande : `hostname`

    - résultat : 
    ```
    node1.tp4.linux
    ```

# II - Mettre en place un service 

## 1. Install

- **Installation de NGINX**

    - commande : `sudo dnf install nginx`

    - résultat : 
    ```
    Installing:
      nginx                              x86_64        1:1.14.1-9.module+el8.4.0+542+81547229         appstream        566 k
    [...]
    Complete!
    ```

    - commande : `sudo systemctl start nginx`

    - commande : `sudo systemctl status nginx`

    - résultat : 
    ```
    [...]
     Active: active (running) since Tue 2021-11-23 12:23:30 CET; 5s ago
    [...]
    ```

## 2. Analyse

- **Analyse du service NGINX**

    - commande : `ps -U root -u root u | grep nginx`

    - résultat : 
    ```
    root       25699  0.0  0.2 119192  2164 ?        Ss   12:23   0:00 nginx: master process /usr/sbin/nginx
    ```

    - commande : `sudo ss -ltpn | grep nginx`

    - résulat : 
    ```
    LISTEN 0      128           0.0.0.0:80          0.0.0.0:*     users:(("nginx",pid=25700,fd=8),("nginx",pid=25699,fd=8))
    ```

    - commande : `ls /etc/nginx/`

    - résultat : 
    ```
    conf.d        fastcgi.conf.default    koi-utf     mime.types.default  scgi_params          uwsgi_params.default
    default.d     fastcgi_params          koi-win     nginx.conf          scgi_params.default  win-utf
    fastcgi.conf  fastcgi_params.default  mime.types  nginx.conf.default  uwsgi_params
    ```

    - commande : `ls -l /etc/nginx`

    - résultat : 
    ```
    [...]
    -rw-r--r--. 1 root root 1077 Jun 10 11:10 fastcgi.conf
    -rw-r--r--. 1 root root 1077 Jun 10 11:10 fastcgi.conf.default
    -rw-r--r--. 1 root root 1007 Jun 10 11:10 fastcgi_params
    -rw-r--r--. 1 root root 1007 Jun 10 11:10 fastcgi_params.default
    [...]
    ```
## 3. Visite du site web

- **Configuration du firewall**

    - commande : `sudo firewall-cmd --add-port=80/tcp --permanent`

    - commande : `sudo firewall-cmd --reload`

    - résultat :
    ```
    public (active)
    target: default
    icmp-block-inversion: no
    interfaces: enp0s3 enp0s8
    sources:
    services: cockpit dhcpv6-client ssh
    ports: 80/tcp
    [...]
    ```

- **Test du bon fonctionnement du service**

    - commande : `curl http://10.200.1.89:80`

    - résultat : 
    ```
    StatusCode        : 200
    StatusDescription : OK
    Content           : <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

                    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
                      <head>
                        <title>Test Page for the Nginx...
    RawContent        : HTTP/1.1 200 OK
    ```
## 4. Modif de la conf du serveur web 

- **Changement du port d'écoute**

    - commande : `sudo nano /etc/nginx/nginx.conf`

    - commande : `cat /etc/nginx/nginx.conf` 

    - résultat : 
    ```
    [...]
    server {
        listen       8080 default_server;
        listen       [::]:8080 default_server;
    [...]
    ```

    - commande : `sudo systemctl restart nginx`

    - commande : `sudo systemctl status nginx`

    - résultat : 
    ```
    [...]
    Active: active (running) since Tue 2021-11-23 16:58:46 CET; 1min 22s ago
    [...]
    ```

    - commande : `sudo ss -ltpun | grep nginx`

    - résultat : 
    ```
    tcp   LISTEN 0      128          0.0.0.0:8080      0.0.0.0:*    users:(("nginx",pid=1640,fd=8),("nginx",pid=1639,fd=8))
    tcp   LISTEN 0      128             [::]:8080         [::]:*    users:(("nginx",pid=1640,fd=9),("nginx",pid=1639,fd=9))
    ```

    - commande : `sudo firewall-cmd --add-port=8080/tcp --permanent`

    - commande : `sudo firewall-cmd --remove-port=80/tcp --permanent`
    
    - commande : `sudo firewall-cmd --reload`

    - commande : `sudo firewall-cmd --list-all`

    - résultat : 
    ```
    [...]
    ports: 8080/tcp
    [...]
    ```

    - commande : `curl http://10.200.1.89:8080`

    - résultat : 
    ```
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
    [...]
    ```

- **Changement de l'utilisateur qui lance le service**

- nouvel utilisateur 

    - commande : `useradd web`

    - commande : `realpath web`
   
    - résultat :
    ```
    /home/web
    ```

- Nouveau mot de passe 

    - commande : `sudo passwd web`

    - résultat : 
    ```
    passwd: all authentication tokens updated successfully.
    ```

- changement d'utilisateur nginx 

    - commande : `sudo nano /etc/nginx/nging.conf`

    - commande : `cat /etc/nginx/nging.conf`

    - résultat : 
    ```
    [...]
    user web;
    [...]
    ```

    - commande : `systemctl restart nginx`

    - commande : `ps -U web -u web u`
    
    - résultat : 
    ```
    web         2378  0.0  1.0 151820  8300 ?        S    18:37   0:00 nginx: worker process
    ```

- **Changement de l'emplacement de la racine web**

    - commande : `sudo mkdir /var/www/`

    - commande : `sudo mkdir /var/www/super_title_web`

    - commande : `sudo nano /var/www/super_title_web/index.html`

    - commande : `cat /var/www/super_titlte_web/index.html`

    - résultat : 
    ```
    <h1>il</h1>
    <h2>s'</h2>
    <h3>appel</h3>
    <h4>toto</h4>
    ```

    - commande : `sudo chown web /var/www/super_title_web/ | sudo chgrp web /var/www/super_title_web/ | sudo chown web
/var/www/super_title_web/index.html | sudo chgrp web /var/www/super_title_web/index.html`

    - commande : `ls -lR /var/www/ | grep web`
    
    - résultat : 
    ```
    drwxr-xr-x. 2 web web 24 Nov 23 18:49 super_title_web
    /var/www/super_title_web:
    -rw-r--r--. 1 web web 53 Nov 23 18:49 index.html
    ```

- nouvelle racine web pour NGINX

    - commande : `sudo nano /etc/nginx/nginx.conf`

    - commande : `cat /etc/nginx/nginx.conf`

    - résultat : 
    ```
    server {
        listen       8080 default_server;
        listen       [::]:8080 default_server;
        server_name  _;
        root         /var/www/super_title_web_index.html;
    ```

    - commande : `systemctl restart nginx`

    - commande : `curl http://10.200.1.89:8080`

    - résultat : 
    ```
    <html>
    <head><title>404 Not Found</title></head>
    <body bgcolor="white">
    <center><h1>404 Not Found</h1></center>
    <hr><center>nginx/1.14.1</center>
    </body>
    </html>
    ```

![](https://memegenerator.net/img/instances/82448307/nginx-fuck-you.jpg)  
