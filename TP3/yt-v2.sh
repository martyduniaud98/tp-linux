#!/bin/bash

fichier="/home/marty/Desktop/video_a_DL.txt"
oldIFS=$IFS     # sauvegarde du séparateur de champ
IFS=$'\n'       # nouveau séparateur de champ, le caractère fin de ligne

while true; 
do
  #si downloads n'existe pas le script exit
  if [[ -d "/srv/yt/downloads/" ]] && [[ -f /var/log/yt/download.log ]]; then

    for ligne in $(cat /"$fichier");
    do

      if [[ $ligne == https://www.youtube.com* ]]; then
	titre=$(youtube-dl -e "$ligne")
	mkdir /srv/yt/downloads/"$titre" #creation directory TITRE_VIDEO 
	youtube-dl -o "/srv/yt/downloads/$titre/%(title)s.%(ext)s" "$ligne" > /dev/null #dl video vers dossier TITRE_VIDEO
	youtube-dl -o "/srv/yt/downloads/$titre/description" "$ligne" --write-description --skip-download --youtube-skip-dash-manifest > /dev/null #DL description video
	echo "Video $titre was downloaded." 
	echo File path : /srv/yt/downloads/"$titre"/"$titre.mp4"
	date_log=$(date "+%y/%m/%d %T") #formatage date 
	echo ["$date_log"] Video "$ligne" was downloaded. File path : /srv/yt/downloads/"$titre"/"$titre.mp4" >> /var/log/yt/download.log
	sed -i 1d /home/marty/Desktop/video_a_DL.txt #delete line

      else
	sed -i 1d /home/marty/Desktop/video_a_DL.txt
      fi

    done

  else
    echo "Il manque le dossier downloads ou le fichier download.log"
    exit
  fi
done
sleep 30s


