# TP3 : A little script

## 1. Script carte d'identité

- Exemple d'execution avec une sortie :

    - commande : `sudo bash /srv/idcard/idcard.sh`

    - résultat : 
    ```
    Machine name : node1.tp2.linux
    OS : Linux and kernel version is 5.11.0-38-generic x86_64
    IP : 192.168.56.115/24
    RAM disponible : 1,4Gi / 1,9Gi
    Disque : 2,3G space left

    Top 5 processes by RAM usage :
    4.2 xfwm4 --replace
    4.0 /usr/lib/xorg/Xorg -core :0 -seat seat0 -auth /var/run/lightdm/root/:0 -nolisten tcp vt7 -novtswitch
    2.2 /usr/bin/python3 /usr/bin/blueman-applet
    2.1 /usr/lib/x86_64-linux-gnu/xfce4/panel/wrapper-2.0 /usr/lib/x86_64-linux-gnu/xfce4/panel/plugins/libwhiskermenu.so 1 16777223 whiskermenu Whisker Menu Show a menu to easily access installed applications
    2.0 xfdesktop

    Listening ports :
    tcp     LISTEN   0        4096       127.0.0.53%lo:53             0.0.0.0:*      users:(("systemd-resolve",pid=409,fd=13))
    tcp     LISTEN   0        5              127.0.0.1:631            0.0.0.0:*      users:(("cupsd",pid=454,fd=7))

    tcp     LISTEN   0        128              0.0.0.0:60000          0.0.0.0:*      users:(("sshd",pid=551,fd=3))

    tcp     LISTEN   0        32                     *:50000                *:*      users:(("vsftpd",pid=539,fd=3))

    tcp     LISTEN   0        5                  [::1]:631               [::]:*      users:(("cupsd",pid=454,fd=6))

    tcp     LISTEN   0        128                 [::]:60000             [::]:*      users:(("sshd",pid=551,fd=4))

    Here's my random cat : https://cdn2.thecatapi.com/images/MTk1Njk4NA.jpg
    ```

## 2. Script youtube-dl

- Exemple d'execution avec une sortie :

    - commande : `./yt.sh https://www.youtube.com/watch?v=qVVKfpc--KA`

    - résultat :
    ```
    marty@node1:/srv/yt$ ./yt.sh https://www.youtube.com/watch?v=qVVKfpc--KA
    Video https://www.youtube.com/watch?v=qVVKfpc--KA was downloaded.
    File path : /srv/yt/downloads/Video conne de 3sec (animation de merde quoi😂)/Video conne de 3sec (animation de merde quoi😂).mp4
    ```

## 3. MAKE IT A SERVICE

- Status service yt

    - commande :`systemctl status yt`

    - résultat : 
    ```
    ● yt.service - Lance yt-v2 for DL list videos
     Loaded: loaded (/etc/systemd/system/yt.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-11-22 20:35:06 CET; 12min ago
    [...]
    ```

- Extrait du journal de log

    - commande : `journalctl -xe -u yt`

    - résultat : 
    ```
    [...]
    nov. 22 20:40:59 node1.tp2.linux yt-v2.sh[603]: Video "Hearts" Making of the Video (:20sec Time-Lapse) was downloaded.
    nov. 22 20:40:59 node1.tp2.linux yt-v2.sh[603]: File path : /srv/yt/downloads/"Hearts" Making of the Video (:20sec Time-Lapse)/"Hearts" Making of the Video (:20sec Time-Lapse).mp4
    nov. 22 20:42:39 node1.tp2.linux yt-v2.sh[603]: Video BREUTON JeanPol 20sec was downloaded.
    nov. 22 20:42:39 node1.tp2.linux yt-v2.sh[603]: File path : /srv/yt/downloads/BREUTON JeanPol 20sec/BREUTON JeanPol 20sec.mp4
    ```

- Commande pour démarrer le service automatiquement au démarrage de la VM

    - commande : `ExecStart=/srv/yt/yt-v2.sh start`

> *Quand il te dit de mettre des asciinema et des trucs blingbling*

![](https://c.tenor.com/W8qooSdJLV4AAAAM/kaamelott-leodagan.gif)
