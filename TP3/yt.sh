#!/bin/bash

#si downloads n'existe pas le script exit
if [[ -d "/srv/yt/downloads/" ]] && [[ -f /var/log/yt/download.log ]]; then
  titre=$(youtube-dl -e "$1") #Dl le titre de la vidéo 
  mkdir /srv/yt/downloads/"$titre" #creation directory TITRE_VIDEO 
  youtube-dl -o "/srv/yt/downloads/$titre/%(title)s.%(ext)s" "$1" > /dev/null #dl video vers dossier TITRE_VIDEO
  youtube-dl -o "/srv/yt/downloads/$titre/description" "$1" --write-description --skip-download --youtube-skip-dash-manifest > /dev/null #DL description video
  echo "Video $1 was downloaded." 
  echo File path : /srv/yt/downloads/"$titre"/"$titre.mp4"

  date_log=$(date "+%y/%m/%d %T") #formatage date 
  echo ["$date_log"] Video "$1" was downloaded. File path : /srv/yt/downloads/"$titre"/"$titre.mp4" >> /var/log/yt/download.log

else
  echo "Il manque le dossier downloads ou le fichier download.log"
  exit
fi




