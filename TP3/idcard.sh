#!/bin/bash
nom_machine=$(hostname)
version_kernel=$(uname -mr) 
OS=$(uname -s)
IP_machine=$(ip a | grep 192 | cut -c 10-27)
RAM_dispo=$(free -mh | grep Mem | cut -d" " -f45-)
RAM_total=$(free -mh | grep Mem | cut -d" " -f10-15)
Disk_left=$(df -h | grep sda5 | cut -d" " -f12)
top5_process=$(ps -A | ps -eo pmem,args | sort -nr | head -n 5)
port_listen=$(ss -luntp | grep LISTEN)
random_cat=$(curl -s https://api.thecatapi.com/v1/images/search | cut -d'"' -f10) #oui oui les cut sont crades :'( 

echo "Machine name : $nom_machine"
echo "OS : $OS and kernel version is $version_kernel"
echo "IP : $IP_machine"
echo "RAM disponible :$RAM_dispo /$RAM_total"
echo -e "Disque : $Disk_left space left \n"

echo "Top 5 processes by RAM usage :"

echo -e "$top5_process \n"

echo "Listening ports :"

#lancement du script avec sudo rend visible les noms des progr. des ports d'écoute
echo "$port_listen"
echo "Here's my random cat : $random_cat"
