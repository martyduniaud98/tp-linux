## II. Setup web <br></br>

## 1. Install Apache <br></br>

### A - APACHE :
### **Installation Apache sur `web.tp5.linux`**<br></br>
```
[marty@web ~]$ sudo dnf install httpd
[sudo] password for marty:
Installing:
 httpd                       x86_64           2.4.37-43.module+el8.5.0+714+5ec56ee8           appstream           1.4 M
Complete!
```
<br></br>
### **Analyse du service Apache** <br></br>

```
[marty@web ~]$ sudo systemctl start httpd
[marty@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
```

```
[marty@web ~]$ ps -ef | grep httpd
root        1928       1  0 09:23 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1929    1928  0 09:23 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1930    1928  0 09:23 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1931    1928  0 09:23 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1932    1928  0 09:23 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
marty       2252    1401  0 09:32 pts/0    00:00:00 grep --color=auto httpd
```

```
[marty@web ~]$ sudo ss -laptn | grep httpd
LISTEN 0      128                *:80              *:*     users:(("httpd",pid=1932,fd=4),("httpd",pid=1931,fd=4),("httpd",pid=1930,fd=4),("httpd",pid=1928,fd=4))
[marty@web ~]$
```

```
[marty@web ~]$ ps -u root | grep httpd
   1928 ?        00:00:00 httpd
```
<br></br>

### **Un premier test** <br></br>

```
[marty@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[marty@db ~]$ sudo firewall-cmd --reload
success
```

- test connexion page d'accueil Apache via PC

```
PS C:\Users\marty> curl http://10.5.1.11:80
curl : HTTP Server Test Page
[...]
```
<br></br>

### B - PHP :
### **Installer PHP** <br></br>

```
[marty@web /]$ sudo dnf install epel-release
[...]
Complete!

[marty@web /]$ sudo dnf update
[...]
Complete!

[marty@web /]$ sudo dnf module enable php:remi-7.4
Complete!

[marty@web /]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
Complete!
```
<br></br>

## 2. Conf Apache <br></br>

### **Analyser la conf Apache** <br></br>

```
[marty@web /]$ cat /etc/httpd/conf/httpd.conf | grep .conf | tail -2
# Load config files in the "/etc/httpd/conf.d" directory, if any.
IncludeOptional conf.d/*.conf
```
<br></br>
### **Creer un VirtualHost qui accueillera NextCloud** <br></br>

```
[marty@web /]$ sudo nano /etc/httpd/conf.d/VirtualHost.conf
[sudo] password for marty:
[marty@web /]$ cat /etc/httpd/conf.d/VirtualHost.conf
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp5.linux

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```
<br></br>

### **Configurer la racine web** <br></br>

```
[marty@web www]$ sudo mkdir /nextcloud/
[marty@web www]$ sudo mkdir /nextcloud/html/
[marty@web www]$ sudo chown apache:apache /nextcloud/html/
[sudo] password for marty:
```
<br></br>

### **Configurer PHP** <br></br>

```
[marty@web var]$ cat /etc/opt/remi/php74/php.ini | grep date.timezone | tail -1
date.timezone = Europe/Paris
```
<br></br>

## 3. Install NextCloud <br></br>

### **Récupérer Nextcloud** <br></br>

```
[marty@web www]$ cd
[marty@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  148M  100  148M    0     0  6601k      0  0:00:22  0:00:22 --:--:-- 6230k
[marty@web ~]$ ls nextcloud-21.0.1.zip
nextcloud-21.0.1.zip
```
<br></br>

### **Ranger la chambre** <br></br>

```
[marty@web ~]$ sudo unzip nextcloud-21.0.1.zip -d /var/www/nextcloud/html/
[...]   
   inflating: /var/www/nextcloud/html/nextcloud/config/.htaccess
```
```
[marty@web ~]$ sudo chown -R apache:apache /var/www/nextcloud/html/nextcloud/
```
```
[marty@web ~]$ rm nextcloud-21.0.1.zip
```
<br></br>

## 4. Test <br></br>

### **Modifier le fichier `hosts` du PC** <br></br>
```
PS C:\> cat C:\Windows\System32\drivers\etc\hosts
[...]
10.5.1.11 web.tp5.linux
```
<br></br>

### **Tester l'accès à NextCloud et finaliser son install'** <br></br>

```
PS C:\> curl web.tp5.linux


StatusCode        : 200
StatusDescription : OK
[...]
```

![](https://memegenerator.net/img/instances/74403175/well-looks-like-we-have-a-winner.jpg)