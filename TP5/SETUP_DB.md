# TP5 P'tit cloud perso

## I. Setup db <br></br>

### **Installation MariaDB** <br></br>

```
sudo dnf install mariadb-server
[sudo] password for marty:
Rocky Linux 8 - AppStream                                                               7.7 kB/s | 4.8 kB     00:00
Rocky Linux 8 - AppStream                                                               3.5 MB/s | 8.2 MB     00:02
[...]
Complete!
``` 

<br></br>
### **Le service mariaDB** <br></br>    
    
```
[marty@db ~]$ sudo systemctl start mariadb
[marty@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
[marty@db ~]$ systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
 Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
 Active: active (running) since Thu 2021-11-25 12:17:22 CET; 1min 39s ago
[...]
```
    
```
[marty@db ~]$ sudo ss -ltupn | grep sql
tcp   LISTEN 0      80                 *:3306            *:*    users:(("mysqld",pid=26265,fd=21))    
```

```
[marty@db ~]$ ps -eF | grep sql
mysql      26265       1  0 324208 89116  0 12:17 ?        00:00:00 /usr/libexec/mysqld --basedir=/usr
marty      26464    1454  0 55482  1088   0 12:28 pts/0    00:00:00 grep --color=auto sql    
```
    
- Le process mariaDB est lancé par mysql 
    
<br></br>

### **Firewall** <br></br>

```
[marty@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[marty@db ~]$ sudo firewall-cmd --reload
success
sudo firewall-cmd --list-all
public (active)
target: default
icmp-block-inversion: no
interfaces: enp0s3 enp0s8
sources:
services: cockpit dhcpv6-client ssh
ports: 3306/tcp
[...]    
```
<br></br>

## 2. Conf mariaDB <br></br>

### **Configuation élémentaire de la base** <br></br>

```
[marty@db ~]$ mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!
```
- en 1er lieu on nous demande de modifier ou créer le password de root (interne à mariaDB).

```
By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!
```
- On choisit de supprimer les users anonymes, ils ont tout les droits dans les bases commençant par test_.
  
```
Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n]
 ... Success!
```
- On refuse la connexion root à distance pour limiter les ravages si une attaque se produit.

```
By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!
```
- On supprime la database test parce que nous allons passer à un environnement de production.
  
```
Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

- On termine par un reload pour prendre en compte les modifications apportées.
<br></br>

### **Préparation de la base en vue de l'utilisation par NewtCloud**<br></br>

```
[marty@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 19
Server version: 10.3.28-MariaDB MariaDB Server
[...]
```
```
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.5.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.5.1.11';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.002 sec)
```
<br></br>

## 3. Test <br></br>

### **Installez sur la machine `web.tp5.linux` la commande `mysql`** <br></br>
```
[marty@web /]$ sudo dnf provides mysql
Last metadata expiration check: 0:16:35 ago on Thu 25 Nov 2021 11:44:40 AM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7
[marty@web /]$ sudo dnf install mysql
[...]
Installed:
  mariadb-connector-c-config-3.1.11-2.e18_3.noarch
  mysql-8.0.26-1.module+e18.4.0+652+6de068a7.x86_64
  mysql-common-8.0.26-1.module+e18.4.0+652+6de068a7.x86_64

Complete!
```
<br></br>

### **Tester la connexion** <br></br>

```
[marty@web ~]$ mysql -h 10.5.1.12 -P 3306 -u nextcloud -p nextcloud

mysql> SHOW TABLES;
Empty set (0.00 sec)
```

- [2 - Setup Web](./SETUP_WEB.md) <br></br>

![](https://starecat.com/content/wp-content/uploads/harold-wrong-database-selected-8388409-rows-affected.jpg)