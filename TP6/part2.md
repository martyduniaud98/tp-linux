# Partie 2 : Setup du serveur NFS sur `backup.tp6.linux` 

<br></br>

### 🌞 Préparer les dossiers à partager <br></br>

- **créez deux sous-dossiers dans l'espace de stockage dédié**
```
[marty@backup /]$ mkdir /backup/web.tp6.linux/
[marty@backup /]$ mkdir /backup/db.tp6.linux/
```
<br></br>

### 🌞 Install du serveur NFS <br></br>

- **installer le paquet `nfs-utils`**
```
[marty@backup /]$ sudo dnf install nfs-utils
[...]
Complete!
```
<br></br>

### 🌞 Conf du serveur NFS <br></br>

- **fichier `/etc/idmapd.conf`**
```
[marty@backup /]$ sudo nano /etc/idmapd.conf
[marty@backup /]$ cat /etc/idmapd.conf | grep "Domain ="
Domain = tp6.linux
```
<br></br>

- **fichier `/etc/exports`**
```
[marty@backup /]$ cat /etc/exports
/backup/web.tp6.linux 10.5.1.0/24(rw,no_root_squash)
/backup/db.tp6.linux 10.5.1.0/24(rw,no_root_squash)
```     
- `rw` : ca signifie droit de lecture et d'écriture
    
- `no_root_squash` : l'utilisateur ne peut pas faire de transformation d'UID (il ne peut pas utiliser `sudo` par exemple)

<br></br>

### 🌞 Démarrer le service <br></br>

- **le service s'appelle `nfs-server`**
```
[marty@backup /]$ systemctl start nfs-server
[...]
==== AUTHENTICATION COMPLETE ====
```
<br></br>

- **après l'avoir démarré, prouvez qu'il est actif**
```
[marty@backup /]$ systemctl status nfs-server
● nfs-server.service - NFS server and services
   Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; disabled; vendor preset: disabled)
   Active: active (exited) since Tue 2021-11-30 13:07:57 CET; 1min 21s ago
[...]
```
<br></br>

- **faites en sorte qu'il démarre automatiquement au démarrage de la machine**
```
[marty@backup /]$ systemctl enable nfs-server
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-unit-files ====
Authentication is required to manage system service or unit files.
Authenticating as: marty
Password:
==== AUTHENTICATION COMPLETE ====
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Authenticating as: marty
Password:
==== AUTHENTICATION COMPLETE ====
```
<br></br>

### 🌞 Firewall <br></br>

- **le port à ouvrir est le `2049/tcp`**
```
[marty@backup /]$ sudo firewall-cmd --add-port=2049/tcp --permanent
[sudo] password for marty:
success
[marty@backup /]$ sudo firewall-cmd --reload
success
```
<br></br>

- **prouver que la machine écoute sur ce port (commande `ss`)**
```
[marty@backup /]$ sudo ss -laptn | grep 2049
LISTEN 0      64           0.0.0.0:2049       0.0.0.0:*
LISTEN 0      64              [::]:2049          [::]:*
```
<br></br>

[la partie 3 : pour setup les clients NFS](./part3.md)