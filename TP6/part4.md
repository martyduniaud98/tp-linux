# Partie 4 : Scripts de sauvegarde
<br>

## I. Sauvegarde Web

### 🌞 Ecrire un script qui sauvegarde les données de NextCloud
<br>

```
[marty@web ~]$ nano save_web.sh
[marty@web ~]$ chmod 774 save_web.sh
[marty@web ~]$ sudo mkdir /var/log/backup
[marty@web ~]$ sudo touch /var/log/backup/backup.log
[marty@web ~]$ sudo chown -R marty:marty /var/log/backup/
[marty@web ~]$ chmod 664 /var/log/backup/backup.log

[marty@web ~]$ ./save_web.sh
Backup /srv/backup/nextcloud_21.12.04_15:46:50.tar.gz created successfully.

[marty@web ~]$ cat /var/log/backup/backup.log
[21/12/04 15:46:50] Backup /srv/backup/nextcloud_21.12.04_15:46:50.tar.gz created successfully.

[marty@web ~]$ cat save_web.sh
#!/bin/bash
#Simple data backup script

#creation of the backup file
yymmdd_hhmmss=$(date "+%y.%m.%d"_"%T")
save_name=(nextcloud_"$yymmdd_hhmmss".tar.gz)
touch /srv/backup/"$save_name"

#log message
date_log=$(date "+%y/%m/%d %T")
success_backup=("Backup /srv/backup/$save_name created successfully.")
echo "[$date_log] $success_backup" >> /var/log/backup/backup.log

#terminal message success backup
echo $success_backup
```
<br>

### 🌞 Créer un service
<br>

- **créer un service backup.service qui exécute votre script**
```
[marty@web ~]$ sudo nano /etc/systemd/system/backup.service
[marty@web ~]$ cat /etc/systemd/system/backup.service
[UNIT]
Description=Lance un backup au lancement du service

[Service]
Type=oneshot
ExecStart=/home/marty/save_web.sh
RemainAfterExit=yes
ExecRestart=/home/marty/save_web.sh restart

[Install]
WantedBy=multi-user.target
```
```
[marty@web ~]$ systemctl restart backup.service
[...]
Dec 04 17:01:27 web.tp6.linux systemd[1]: Starting Lance un backup au lancement du service...
Dec 04 17:01:27 web.tp6.linux save_web.sh[1765]: Backup /srv/backup/nextcloud_21.12.04_17:01:27.tar.gz created successfully.
Dec 04 17:01:27 web.tp6.linux systemd[1]: Started Lance un backup au lancement du service.
```
<br>

### 🌞 Créer un timer
<br>

- **un timer c'est un fichier qui permet d'exécuter un service à intervalles réguliers**
- **créez un timer qui exécute le service backup toutes les heures**
```
[marty@web ~]$ sudo systemctl daemon-reload
[marty@web ~]$ sudo systemctl start backup.timer
[marty@web ~]$ sudo systemctl enable backup.timer
Created symlink /etc/systemd/system/timers.target.wants/backup.timer → /etc/systemd/system/backup.timer.
[marty@web ~]$ sudo systemctl list-timers | grep backup
Sun 2021-12-05 13:00:00 CET  17min left n/a                          n/a       backup.timer                 backup.service
```
<br>

## II. Sauvegarde base de données
<br>

### 🌞 Ecrire un script qui sauvegarde les données de la base de données MariaDB