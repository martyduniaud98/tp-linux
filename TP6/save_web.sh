#!/bin/bash
#Simple data backup script 

#creation of the backup file
yymmdd_hhmmss=$(date "+%y.%m.%d"_"%T")
save_name=(nextcloud_"$yymmdd_hhmmss".tar.gz)
touch /srv/backup/"$save_name"

#log message
date_log=$(date "+%y/%m/%d %T")
success_backup=("Backup /srv/backup/$save_name created successfully.")
echo "[$date_log] $success_backup" >> /var/log/backup/backup.log

#terminal message success backup
echo $success_backup
