# Partie 3 : Setup des clients NFS : `web.tp6.linux` et `db.tp6.linux`

<br></br>

### 🌞 Install'

- **le paquet à install pour obtenir un client NFS c'est le même que pour le serveur : `nfs-utils`**
```
[marty@web ~]$ sudo dnf install nfs-utils
[sudo] password for marty:
[...]
Complete!
```
<br></br>

### 🌞 Conf'

- **créer un dossier `/srv/backup` dans lequel sera accessible le dossier partagé**
```
[marty@web ~]$ sudo mkdir /srv/backup
```
<br></br>

- **pareil que pour le serveur : fichier `/etc/idmapd.conf`**
```
[marty@web ~]$ sudo nano /etc/idmapd.conf
[marty@web ~]$ cat /etc/idmapd.conf | grep "Domain ="
Domain = tp6.linux
``` 
<br></br>

### 🌞 Montage !

- **monter la partition NFS `/backup/web.tp6.linux/` avec une comande `mount`**
```
[marty@web ~]$ sudo mount -t nfs 10.5.1.13:/backup/web.tp6.linux /srv/backup
[marty@web ~]$ df -h | grep web
10.5.1.13:/backup/web.tp6.linux  6.2G  2.1G  4.2G  34% /srv/backup
[marty@web /]$ ls -l /srv/
total 0
drwxrwxr-x. 2 marty marty 6 Nov 30 12:42 backup
```
<br></br>

- **définir un montage automatique de la partition (fichier `/etc/fstab`)**
```
[marty@web ~]$ cat /etc/fstab | grep web.tp6
10.5.1.13:/backup/web.tp6.linux /srv/backup nfs defaults 0 0
[marty@web ~]$ sudo umount /srv/backup
[marty@web ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount.nfs: timeout set for Fri Dec  3 11:06:57 2021
mount.nfs: trying text-based options 'vers=4.2,addr=10.5.1.13,clientaddr=10.5.1.11'
/srv/backup              : successfully mounted
[marty@web ~]$
```
<br></br>

### 🌞 Répétez les opérations sur `db.tp6.linux`

- **le point de montage sur la machine db.tp6.linux est aussi sur `/srv/backup`**
- **le dossier à monter est `/backup/db.tp6.linux/`**
```
[marty@db ~]$ sudo mount -t nfs 10.5.1.13:/backup/db.tp6.linux /srv/backup
```
<br></br>

- **vous ne mettrez dans le compte-rendu pour `db.tp6.linux` que les preuves de fonctionnement**
```
[marty@db srv]$ df -h | grep db
10.5.1.13:/backup/db.tp6.linux  6.2G  2.1G  4.2G  34% /srv/backup
[marty@db srv]$ ls -l /srv/
total 0
drwxrwxr-x. 2 marty marty 6 Nov 30 12:43 backup
[marty@db srv]$ sudo umount /srv/backup
[marty@db srv]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount.nfs: timeout set for Fri Dec  3 11:29:44 2021
mount.nfs: trying text-based options 'vers=4.2,addr=10.5.1.13,clientaddr=10.5.1.12'
/srv/backup              : successfully mounted
```
<br></br>
[la partie 4 : Scripts de sauvegarde](./part4.md)
<br></br>

![](https://img.20mn.fr/79x6euBVSZqUU9kBE2bPFg/310x190_slug%20magazine.jpg)