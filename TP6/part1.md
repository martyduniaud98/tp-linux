# Partie 1 : Préparation de la machine backup.tp6.linux 

<br></br>

## I. Ajout de disque 

### 🌞 Ajouter un disque dur de 5Go à la VM `backup.tp6.linux` 
```
[marty@backup ~]$ lsblk | grep sdb
sdb           8:16   0    5G  0 disk
```
<br></br>

## II. Partitioning 
### 🌞 Partitionner le disque à l'aide de LVM <br></br>

- **créer un physical volume (PV) : le nouveau disque ajouté à la VM**
```
[marty@backup ~]$ sudo pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.
[marty@backup ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  rl lvm2 a--  <7.00g    0
  /dev/sdb      lvm2 ---   5.00g 5.00g
```
<br></br>

- **créer un nouveau volume group (VG)**
```
[marty@backup ~]$ sudo vgs | grep backup
  backup   1   0   0 wz--n- <5.00g <5.00g
```
 <br></br>

- **créer un nouveau logical volume (LV) : ce sera la partition utilisable**
```
[marty@backup ~]$ sudo lvcreate -l 100%FREE backup -n ma_partition
Logical volume "ma_partition" created.
``` 
<br></br>

### 🌞 Formater la partition <br></br>
- **formater la partition en ext4 (avec une commande `mkfs`)**  
```
[marty@backup ~]$ sudo mkfs -t ext4 /dev/backup/ma_partition
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 1309696 4k blocks and 327680 inodes
Filesystem UUID: d9db6004-1dd0-441e-860d-561a02c4bb8d
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done
```
<br></br>

### 🌞 Monter la partition <br></br>

- **montage de la partition (avec la commande `mount`)**
```
[marty@backup ~]$ df -h | grep backup
/dev/mapper/backup-ma_partition  4.9G   20M  4.6G   1% /backup/data1
[marty@backup ~]$ ls -l /backup/
total 4
drwxr-xr-x. 3 marty marty 4096 Nov 30 12:19 data1
```
<br></br>

- **définir un montage automatique de la partition (fichier `/etc/fstab`)**
```
[marty@backup ~]$ sudo nano /etc/fstab
[marty@backup ~]$ sudo umount /backup/data1
[marty@backup ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount: /backup/data1 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/backup/data1            : successfully mounted
```

<br></br>

[la partie 2 : installer un serveur NFS](./part2.md)