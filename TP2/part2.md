# Partie 2 : FTP

## Setup du service FTP

- **1 - Installation du serveur**

    - commande : `sudo apt install vsftpd`

    - résultat : 
    ```
    [...]
    Created symlink /etc/systemd/system/multi-user.target.wants/vsftpd.service - /lib/system/vsftpd.service.
    Processing triggers for man-db (2.9.1-1) ...
    Processing triggers for systemd (245.4-4ubuntu3.11) ...
    ```

- **2 - Lancement du service FTP**

    - commande : `systemctl start vsftpd` puis `systemctl status vsftpd`

    - résultat : 
    ```
    [...]
    Active: active (running) since Mon 2021-11-01 15:59:30 CET; 7min ago
    Main PID: 1878 (vsftpd)
    [...]
    ```

- **3 - Etude du service FTP**

- affichage des processus liés au service ftp

    - commande `ps 1878`

    > *On trouve le PID dans l'onglet de status du serveur ftp*

    - résultat : 
    ```
    PID  TTY     STAT   TIME COMMAND
    1878 ?       Ss     0:00 /usr/sbin/vsftpd /etc/vsftpd.conf
    ```

- affichage du port utilisé par le service ftp

    - commande : `ss -ltpn`

    - résultat : 
    ```
    [...]
    LISTEN  0     32            *:21             *:*     users:(("vsftpd",pid=1878,fd=3))
    [...]
    ```

- affichage des logs du service ftp

    - commande : `cd var/log/` puis `journalctl -xe`

    - résultat : 
    ```
    [...]
    nov. 01 17:18:08 node1.tp2.linux sudo [17243]: pam_unix(sudo:session): session opened for user root 
    by (uid=0)
    nov. 01 17:18:08 node1.tp2.linux sudo [17243]: pam_unix(sudo:session): session closed for user root
    ```

- connexion au serveur via le PC en temps que client ftp

    > *Utilisation de Filezilla pour upload/download*
    
    - commande : `sudo cat vsftpd.log`

    - résultat : 
    ```
    Mon Nov  1 18:23:16 2021 [pid 17606] [marty] OK DOWNLOAD: Client "::ffff:192.168.56.1", "/home/marty/
    Desktop/tpLINUX.txt", 11 bytes, 4.61Kbyte/sec
    ```

    - commande : `sudo nano vsftpd.conf` puis `cat vsftpd.conf`

    - résultat :
    ```
    [...]
    # Allow anonymous FTP? (Disabled by default).
    anonymous_enable=YES
    #
    # Uncomment this to enable any form of FTP write command.
    write_enable=YES
    [...]
    ```

    - commande : `sudo cat vsftpd.log`

    - résultat :
    ```
    [...]
    Mon Nov  1 18:36:57 2021 [pid 17692] [marty] OK UPLOAD: Client "::ffff:192.168.56.1", "/home/marty/
    Desktop/POIDS + taille.txt", 29 bytes, 4.99Kbyte/sec
    ```

> *Quand t'es un peu trop fier d'avoir réussi cette étape après 2h de dur labeur*

![](https://femmedinfluence.fr/wp-content/uploads/2016/10/beyonce.gif)




- **4 - Modification de la configuration du serveur**

- changement du port d'écoute vsftpd

    - commande : `sudo nano vsftpd.conf` puis ajouter `listen_port=50000`

    - commande : `cat vsftpd.conf`
    
    - résultat : 
    ```
    [...]
    #utf8_filesystem=YES
    listen_port=50000
    ```
- restart du ftp

    - commande : `sudo systemctl restart ssh` puis `sudo ss -ltpn`

    - résultat : 
    ``` 
    [...]
    LISTEN  0      32       *:50000            *:*     users:(("vsftpd",pid=17797,fd=3))
    [...]
    ```

- test UP/DL sur nouveau port

    - commande : `sudo cat vsftpd.log`

    - résultat :
    ```
    Mon Nov  1 19:17:04 2021 [pid 17826] CONNECT: Client "::ffff:192.168.56.1"
    Mon Nov  1 19:17:04 2021 [pid 17826] [marty] OK LOGIN: Client "::ffff:192.168.56.1"
    Mon Nov  1 19:17:07 2021 [pid 17827] [marty] OK DOWNLOAD: Client "::ffff:192.168.56.1", "/home/marty/
    Desktop/POIDS + taille.txt", 29 bytes, 4.54Kbyte/sec
    Mon Nov  1 19:17:07 2021 [pid 17830] [marty] OK UPLOAD: Client "::ffff:192.168.56.1", "/home/marty/
    Desktop/POIDS + taille.txt", 29 bytes, 20.45Kbyte/sec
    ```

    - [Partie 3 : Création de mon propre service](./part3.md)
