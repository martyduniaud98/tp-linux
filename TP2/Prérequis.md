# TP2 : Explorer et manipuler le système

# Prérequis

## 1. Une machine xubuntu fonctionnelle 

    `marty@marty:~$`

## 2. Nommer la machine

- **première étape : changer le nom tout de suite, jusqu'à ce qu'on redémarre la machine**

    - commande : `sudo hostname node1.tp2.linux`

    - résultat : `marty@node1:~$`

- **deuxième étape : changer le nom qui est pris par la machine quand elle s'allume**

    - commande : `sudo nano /etc/hostname`

      (remplacer texte par : `node1.tp2.linux`)

    - commandes : `sudo systemctl restart ssh` puis : ` cat /etc/hostname`

    - résultat : `node1.tp2.linux`

## 3. Config réseau

🌞 **Config réseau fonctionnelle**

- depuis la VM : `ping 1.1.1.1` fonctionnel

    - commande : `ping 1.1.1.1`

    - résultat : 
    ```
    PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
    64 bytes from 1.1.1.1: icmp_seq=1 ttl=53 time=374 ms
    64 bytes from 1.1.1.1: icmp_seq=2 ttl=53 time=294 ms
    64 bytes from 1.1.1.1: icmp_seq=3 ttl=53 time=33.5 ms
    64 bytes from 1.1.1.1: icmp_seq=4 ttl=53 time=32.2 ms
    ```

- depuis la VM : `ping ynov.com` fonctionnel

    - commande : `ping ynov.com`

    - résultat : 
    ```
    PING ynov.com (92.243.16.143) 56(84) bytes of data.
    64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=50 time=32.0 ms
    64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=50 time=225 ms
    64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=3 ttl=50 time=67.8 ms
    ```

- depuis le PC : `ping <IP_VM>` fonctionnel (adresse : 192.168.56.1)

    - commande : `ping 192.168.56.1`

    - résultat : 
    ```
    Envoi d’une requête 'Ping'  192.168.56.1 avec 32 octets de données :
    Réponse de 192.168.56.1 : octets=32 temps<1ms TTL=128
    Réponse de 192.168.56.1 : octets=32 temps<1ms TTL=128
    Réponse de 192.168.56.1 : octets=32 temps<1ms TTL=128
    Réponse de 192.168.56.1 : octets=32 temps<1ms TTL=128
    [...]
    ```

- [Partie 1 : Installation et configuration d'un service SSH](./part1.md)
