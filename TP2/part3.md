# Partie 3 : Création de mon propre service

## l - Jouer avec netcat

- **Installation de `netcat`**

    - commande : `sudo apt install netcat`

    - résultat :
    ```
    [...]
    Unpacking netcat (1.206-1ubuntu1) ...
    Setting up netcat (1.206-1ubuntu1) ...
    ```

- **Chat établit entre VM et PC**

    > *Connexion PC en temps que client ssh*
   
    - commande VM : `nc -l 2000`

    - commande PC : `netcat 192.168.56.114 2000` 

> *Je parle à présent avec moi*

![](https://c.tenor.com/BaaXPhZ6vr8AAAAM/crazy-insane.gif)


- **Utilisation de `netcat` pour stocker les données échangées dans un fichier**

    - commande VM : `echo 'tp_relou' > /tmp/file1 | nc 192.168.56.114 2000` puis sur le PC `cat file1`

    - résultat : 
    ```
    marty@node1:/tmp$ cat file1
    tp_relou
    ```

## ll - Un service basé sur netcat 

- **1 - Créer le service**

- Création du nouveau service 

    - commande : `sudo nano /etc/systemd/system/chat_tp2.service` suivis de `sudo chmod 777 chat_tp2.service`

    - résultat : 
    ``` 
    -rwxrwxrwx 1 root root   5 nov.   2 14:37 chat_tp2.service 
    ```

- modification du service 

    - commande : `nano /etc/systemd/system/chat_tp2.service`              
    - commande : `cat chat_tp2.service`              
    - commande : `sudo systemctl daemon-reload` 

    - résultat : 
    ```
    [Unit]
    Description=Little chat service (TP2)

    [Service]
    ExecStart=/usr/bin/nb -l 2000

    [Install]
    WantedBy=multi-user.target
    ```

- **2 - Test test et restest**

    - commande : `systemctl start chat_tp2`
    - commande : `systemctl status chat_tp2`

    - résultat : 
    ```
    [...]
    Active: active (running) since Tue 2021-11-02 15:14:43 CET; 2s ago
    [...]
    ```

    - commande : `sudo ss -lptn`

    - résultat : 
    ```
    LISTEN 0      1            0.0.0.0:2000       0.0.0.0:*     users:(("nc",pid=2002,fd=3))   
    [...]
    ```

    - commande test PC : `nc 192.168.56.114 2000`

    - résultat : 
    ```
    marty@node1:/$ nc 192.168.56.114 2000
    test 
    test ok
    ```

    - commande VM : `journalctl -xe -u chat_tp2 -f`

    - résultat :
    ```
    [...]
    nov. 02 15:18:42 node1.tp2.linux nc [2002]: test
    nov. 02 15:18:44 node1.tp2.linux nc [2002]: test ok
    nov. 02 15:20:57 node1.tp2.linux nc [2002]: really 
    nov. 02 15:21:01 node1.tp2.linux nc [2002]: yes really good
    ```
![enfin terminé](https://c.tenor.com/I2e6QCO1HCAAAAAC/jenaimarre-jam.gif)

