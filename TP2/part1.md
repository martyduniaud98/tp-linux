# Partie 1 : SSH 

## l. Setup du serveur SSH

- **1 - Installation du serveur**

    - commande : `sudo apt install openssh-server`

    - résultat : 
    ```Reading package lists... Done
    Building dependency tree
    Reading state information... Done
    openssh-server is already the newest version (1:8.2p1-4ubuntu0.3).
    0 upgraded, 0 newly installed, 0 to remove and 89 not upgraded.```

- **2 - Lancement du service SSH**

    - commande : `systemctl start sshd` puis `systemctl status sshd`

    - résultat : 
    ```ssh.service - OpendBSD Secure Shell server
    Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
    Active: active (running) since Mon 2021-11-01 14:44:25 CET; 12min ago
    [...]```

- **3 - Etude du service SSH**

- affichage des processus liés au service ssh

    - commande `ps ssh`

    - résultat : 
    ```
    1000    1157  00000000  00010000  00380004  4b817efb Ss   pts/0     0:00 bash
    1000    1371  00000000  00000000  00000000 <f3d1fef9 R+   pts/0     0:00 ps ssh
    ```

- affichage du port utilisé par le service ssh

    - commande : `ss -ltpn`

    - résultat : 
    ```
    [...]
    LISTEN  0       128            0.0.0.0:22          0.0.0.0:*      users:(("sshd",pid=513,fd=3))
    [...]
    ```

- affichage des logs du service ssh

    - commande : `cd var/log/` puis `journalctl -xe`

    - résultat : 
    ```
    v. 01 15:17:01 node1.tp2.linux CRON[1411]: pam_unix (cron:session): session closed for user root
    v. 01 15:18:32 node1.tp2.linux systemd[1]: Starting Ubuntu Advantage APT and MOTD Messages...
    [...]
    ```

- connexion au serveur via le PC en temps que client ssh

    - commande : `ssh marty@192.168.56.113`

    - résultat : 
    ```
    Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.11.0-38-generic x86_64)
    [...]
    ```

- **4 - Modification de la configuration du serveur**

- changement du port d'écoute ssh

    - commande : `sudo chmod 777 sshd_config` puis `nano sshd_config`

    > *On remplace le port 22 par 60000*

    - commande : `cat sshd_config`
    
    - résultat : 
    ```
    [...]
    Port 60000
    #AddressFamily any
    #ListenAddress 0.0.0.0
    #ListenAddress ::
    [...]
    ```
- restart du ssh

    - commande : `sudo systemctl restart ssh` puis `ss -latpn`

    - résultat : 
    ```
    State        Recv-Q       Send-Q               Local Address:Port                Peer Address:Port     Process
    LISTEN       0            4096                 127.0.0.53%lo:53                       0.0.0.0:*
    LISTEN       0            5                        127.0.0.1:631                      0.0.0.0:*
    LISTEN       0            128                        0.0.0.0:60000                    0.0.0.0:*
    ESTAB        0            0                  192.168.56.113:22                  192.168.56.1:52978
    LISTEN       0            5                            [::1]:631                         [::]:*
    LISTEN       0            128                           [::]:60000                       [::]:*
    ```

- connexion vers le nouveau port choisi

    - commande : `ssh -p60000 192.168.56.113`

    - résultat : 
    ```
    [...]
    Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.11.0-38-generic x86_64)
    [...]
    ```

- [Partie 2 : Installation et configuration d'un service FTP](./part2.md)
